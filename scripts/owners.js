require('dotenv').config();
const { ethers } = require('hardhat');
const { initializeFile, writeFileAsync } = require('../lib/fileUtils');
const { MoonCatAcclimator, MoonCatLootprints, MoonCatRescue } = require('@mooncatrescue/contracts/moonCatUtils');
let MOONCAT_ACCLIMATOR_ADDRESS = MOONCAT_LOOTPRINTS_ADDRESS = MOONCATRESCUE_ADDRESS = false;

const { FORK_BLOCK } = process.env;

function assignOwner(ownersObj, owner, tokenId) {
  if (typeof ownersObj[owner] == 'undefined') {
    ownersObj[owner] = [tokenId];
  } else if (ownersObj[owner].indexOf(tokenId) < 0) {
    ownersObj[owner].push(tokenId);
  }
}

function doWildBatch(moonCats) {
  let promises = moonCats.map(moonCatData => {
    return ethers.provider.getStorageAt(MOONCATRESCUE_ADDRESS, moonCatData.ownerSlot)
  });
  return Promise.all(promises).then(rs => {
    return rs.map(storageData => {
      return ethers.utils.defaultAbiCoder.decode(['address'], storageData)[0];
    });
  });
}

async function main() {
  MOONCATRESCUE_ADDRESS = (await MoonCatRescue).address;
  MOONCAT_ACCLIMATOR_ADDRESS = (await MoonCatAcclimator).address;
  MOONCAT_LOOTPRINTS_ADDRESS = (await MoonCatLootprints).address;
  JUMPPORT_ADDRESS = '0xF4d150F3D03Fa1912aad168050694f0fA0e44532';

  const BulkOwners = await ethers.getContractFactory('BulkOwners');
  const BULK = await BulkOwners.deploy();

  const JUMPPORT = new ethers.Contract(
    JUMPPORT_ADDRESS,
    [
      'function ownersOf(address tokenAddress, uint256 tokenSearchStart, uint256 tokenSearchEnd) external view returns (address[])'
    ],
    ethers.provider
  )

  const WILDMOONCAT_OWNERS_FILE = `${MOONCATRESCUE_ADDRESS}_${FORK_BLOCK}_owners.json`;
  const MOONCAT_OWNERS_FILE = `${MOONCAT_ACCLIMATOR_ADDRESS}_${FORK_BLOCK}_owners.json`;
  const LOOTPRINT_OWNERS_FILE = `${MOONCAT_LOOTPRINTS_ADDRESS}_${FORK_BLOCK}_owners.json`;

  const MOONCATRESCUE_MEMORY_SLOTS_FILE = `${MOONCATRESCUE_ADDRESS}_memory.json`;
  let moonCatRescue_memory = require('../' + MOONCATRESCUE_MEMORY_SLOTS_FILE);

  // If the files don't exist, start them with an empty object
  initializeFile(WILDMOONCAT_OWNERS_FILE);
  initializeFile(MOONCAT_OWNERS_FILE);
  initializeFile(LOOTPRINT_OWNERS_FILE);

  let wildmooncat_owners = require('../' + WILDMOONCAT_OWNERS_FILE);
  let mooncat_owners = require('../' + MOONCAT_OWNERS_FILE);
  let lootprint_owners = require('../' + LOOTPRINT_OWNERS_FILE);

  let wildMoonCatCount = 0;
  let moonCatCount = 0;
  let lootprintCount = 0;

  async function showData() {
    console.log(`Wild MoonCat Owners: ${Object.keys(wildmooncat_owners).length}, Total MoonCats: ${wildMoonCatCount}`);
    console.log(`MoonCat Owners: ${Object.keys(mooncat_owners).length}, Total MoonCats: ${moonCatCount}`);
    console.log(`lootprint Owners: ${Object.keys(lootprint_owners).length}, Total lootprints: ${lootprintCount}`);
    console.log('');

    await writeFileAsync(WILDMOONCAT_OWNERS_FILE, JSON.stringify(wildmooncat_owners, null, 2));
    await writeFileAsync(MOONCAT_OWNERS_FILE, JSON.stringify(mooncat_owners, null, 2));
    await writeFileAsync(LOOTPRINT_OWNERS_FILE, JSON.stringify(lootprint_owners, null, 2));
  }

  const CHUNK_SIZE = 100;
  for (let i = 0; i < 25600; i += CHUNK_SIZE) {
    console.log(`At ${i}...`);
    await showData();

    console.time('wild batch');
    let wildOwners = await doWildBatch(moonCatRescue_memory.slice(i, i + CHUNK_SIZE));
    console.timeEnd('wild batch');
    console.time('acclimated batch');
    let acclimatedOwners = await BULK.ownersOf(MOONCAT_ACCLIMATOR_ADDRESS, i, i + CHUNK_SIZE);
    console.timeEnd('acclimated batch');

    let jumpPortOwners = [];
    if (FORK_BLOCK > 15008555) {
      jumpPortOwners = await JUMPPORT.ownersOf(MOONCAT_ACCLIMATOR_ADDRESS, i, i + CHUNK_SIZE);
    }

    wildOwners.forEach((owner, index) => {
      if (owner == MOONCAT_ACCLIMATOR_ADDRESS) {
        if (acclimatedOwners[index] == JUMPPORT_ADDRESS) {
          assignOwner(mooncat_owners, jumpPortOwners[index], i + index);
        } else {
          assignOwner(mooncat_owners, acclimatedOwners[index], i + index);
        }
        moonCatCount++;
      } else if (owner != ethers.constants.AddressZero) {
        assignOwner(wildmooncat_owners, owner, i + index);
        wildMoonCatCount++;
      }
    });

    console.time('lootprints batch');
    let owners = await BULK.ownersOf(MOONCAT_LOOTPRINTS_ADDRESS, i, i + CHUNK_SIZE);
    owners.forEach((owner, index) => {
      if (owner != ethers.constants.AddressZero) {
        assignOwner(lootprint_owners, owner, i + index);
        lootprintCount++;
      }
    });
    console.timeEnd('lootprints batch');
    console.log('');
  }
  console.log('Done!');
  await showData();
}

main()
.then(() => process.exit(0))
.catch(error => {
  console.error(error);
  process.exit(1);
});
