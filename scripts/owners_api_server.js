require('dotenv').config();
const { ethers } = require('hardhat');
const { initializeFile, writeFileAsync } = require('../lib/fileUtils');
const { MoonCatRescue, MoonCatAcclimator, JumpPort } = require('@mooncatrescue/contracts/moonCatUtils');
const { FORK_BLOCK } = process.env;

let MOONCATRESCUE_ADDRESS = false;
let MOONCAT_ACCLIMATOR_ADDRESS = false;

function doWildBatch(moonCats) {
  let promises = moonCats.map(moonCatData => {
    return ethers.provider.getStorageAt(MOONCATRESCUE_ADDRESS, moonCatData.ownerSlot)
  });
  return Promise.all(promises).then(rs => {
    return rs.map(storageData => {
      return ethers.utils.defaultAbiCoder.decode(['address'], storageData)[0];
    });
  });
}

// Generate an owners snapshot file, used to bootstrap information on the Data API server
async function main() {
  MOONCATRESCUE_ADDRESS = (await MoonCatRescue).address;
  MOONCAT_ACCLIMATOR_ADDRESS = (await MoonCatAcclimator).address;
  const JUMPPORT = await JumpPort;

  const OWNERS_FILE = 'mooncat_owners.json';
  const ACCLIMATED_OWNERS_FILE = 'mooncat-acclimated_owners.json';
  const JUMPPORT_OWNERS_FILE = 'mooncat-jumpport_owners.json';

  initializeFile(OWNERS_FILE);
  initializeFile(ACCLIMATED_OWNERS_FILE);
  initializeFile(JUMPPORT_OWNERS_FILE);
  let moonCatOwners = require('../' + OWNERS_FILE);
  const moonCatRescue_memory = require(`../${MOONCATRESCUE_ADDRESS}_memory.json`);
  let acclimatedOwners = require('../' + ACCLIMATED_OWNERS_FILE);
  let jumpOwners = require('../' + JUMPPORT_OWNERS_FILE);

  const BulkOwners = await ethers.getContractFactory('BulkOwners');
  const BULK = await BulkOwners.deploy();
  const CHUNK_SIZE = 100;
  const blockNumber = Number(FORK_BLOCK);

  async function parseBatch(dataPromise, ownersArray, loopIndex) {
    let data = await dataPromise;
    data.forEach((owner, index) => {
      let rescueOrder = loopIndex + index;
      if (owner == '0x0000000000000000000000000000000000000000') owner = null; // Reduce filesize of output files
      ownersArray[rescueOrder] = {
        owner: owner,
        block: blockNumber
      };
    });
  }

  for (let i = 0; i < 25440; i+= CHUNK_SIZE) {
    console.log(`At ${i}...`);

    await parseBatch(doWildBatch(moonCatRescue_memory.slice(i, i + CHUNK_SIZE)), moonCatOwners, i);
    await writeFileAsync(OWNERS_FILE, JSON.stringify(moonCatOwners, null, 2));

    await parseBatch(BULK.ownersOf(MOONCAT_ACCLIMATOR_ADDRESS, i, i + CHUNK_SIZE), acclimatedOwners, i);
    await writeFileAsync(ACCLIMATED_OWNERS_FILE, JSON.stringify(acclimatedOwners, null, 2));

    await parseBatch(JUMPPORT.ownersOf(MOONCAT_ACCLIMATOR_ADDRESS, i, i + CHUNK_SIZE), jumpOwners, i);
    await writeFileAsync(JUMPPORT_OWNERS_FILE, JSON.stringify(jumpOwners, null, 2));
  }
  await writeFileAsync(OWNERS_FILE, JSON.stringify(moonCatOwners, null, 2));
  console.log('Done!');
}

main()
.then(() => process.exit(0))
.catch(error => {
  console.error(error);
  process.exit(1);
});
