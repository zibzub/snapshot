require('dotenv').config();
const fs = require('fs');
const { ethers } = require('hardhat');
const { FORK_BLOCK } = process.env;
const { initializeFile, writeFileAsync } = require('../lib/fileUtils');

const MOONCATPOP_ADDRESS = '0xb8c11beda7142ae7986726247f548eb0c3cde474';
const MOONCATPOPVM_ADDRESS = '0x09C61c41C8C5D378CAd80523044C065648Eaa654';

function assignOwner(ownersObj, owner, tokenId) {
  if (typeof ownersObj[owner] == 'undefined') {
    ownersObj[owner] = [tokenId];
  } else if (ownersObj[owner].indexOf(tokenId) < 0) {
    ownersObj[owner].push(tokenId);
  }
}

async function main() {
  const BulkOwners = await ethers.getContractFactory('BulkOwners');
  const BULK = await BulkOwners.deploy();

  const POP_OWNERS_FILE = `${MOONCATPOP_ADDRESS}_${FORK_BLOCK}_owners.json`;
  const VM_OWNERS_FILE = `${MOONCATPOPVM_ADDRESS}_${FORK_BLOCK}_owners.json`;

  // If the files don't exist, start them with an empty object
  initializeFile(POP_OWNERS_FILE);
  initializeFile(VM_OWNERS_FILE);

  let pop_owners = require('../' + POP_OWNERS_FILE);
  let vm_owners = require('../' + VM_OWNERS_FILE);

  let popCount = 0;
  let vmCount = 0;

  async function showData() {
    console.log(`Pop Owners: ${Object.keys(pop_owners).length}, Total Pop cans: ${popCount}`);
    console.log(`Vending Machine Owners: ${Object.keys(vm_owners).length}, Total vending machines: ${vmCount}`);
    console.log('');

    await writeFileAsync(POP_OWNERS_FILE, JSON.stringify(pop_owners, null, 2));
    await writeFileAsync(VM_OWNERS_FILE, JSON.stringify(vm_owners, null, 2));
  }

  const CHUNK_SIZE = 250;
  for (let i = 0; i < 8600; i += CHUNK_SIZE) {
    console.log(`At ${i}...`);
    await showData();

    console.time('pop batch');
    let owners = await BULK.ownersOf(MOONCATPOP_ADDRESS, i, i + CHUNK_SIZE);
    owners.forEach((owner, index) => {
      if (owner != ethers.constants.AddressZero) {
        assignOwner(pop_owners, owner, i + index);
        popCount++;
      }
    });
    console.timeEnd('pop batch');

    if (i < 256) {
      console.time('vm batch');
      let owners = await BULK.ownersOf(MOONCATPOPVM_ADDRESS, i, i + CHUNK_SIZE);
      owners.forEach((owner, index) => {
        if (owner != ethers.constants.AddressZero) {
          assignOwner(vm_owners, owner, i + index);
          vmCount++;
        }
      });
      console.timeEnd('vm batch');
    }
    console.log('');

  }
  console.log('Done!');
  await showData();
}

main()
.then(() => process.exit(0))
.catch(error => {
  console.error(error);
  process.exit(1);
});
