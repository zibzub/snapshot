require('dotenv').config();
const { ethers } = require('hardhat');
const { MoonCatRescue } = require('@mooncatrescue/contracts/moonCatUtils');

let startTime;
let inProgress = false;

function markTime() {
  if (!inProgress) return;
  let now = new Date().getTime();
  let delta = now - startTime;
  let deltaMin = (delta / 1000 / 60).toFixed(2);
  console.log(`${deltaMin} minutes...`);
  setTimeout(markTime, 30000);
}

/**
 * Try using the original "getCatOwners" function on the MoonCatRescue contract.
 * That function attempts to return the entire ownership array in one function call.
 * Since the collection is now fully-rescued, that attempts to fetch a 25,440-item
 * array and return it (which is huge!).
 * 
 * The way that works being called inside a Hardhat session (pinned to a specific block
 * height) is that Hardhat makes many "getStorageAt" calls to the configured RPC endpoint.
 * It needs over 25,000 individual "getStorageAt" calls to accomplish this, which can take
 * hours in real-time.
 * 
 * So, using this script is not practical, but it does illustrate how the data calls are being
 * made. It's more effective to enumerate ownership by using "getStorageAt" directly
 * (allows for more insight into progress being made, and optionally querying subsets of the
 * collection rather than the whole thing). See the "rescue_state.js" and "owners.js" scripts
 * for details on that method.
 */
async function main() {
  const RESCUE = await MoonCatRescue;
  console.log('Starting call...');
  startTime = new Date().getTime();
  inProgress = true;
  setTimeout(markTime, 30000);
  let data = await RESCUE.getCatOwners();
  inProgress = false;
  console.log(data);
  console.log('Done!');
}

main()
.then(() => process.exit(0))
.catch(error => {
  console.error(error);
  process.exit(1);
});
