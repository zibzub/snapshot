require('dotenv').config();
const { ethers } = require('hardhat');
const { MoonCatAccessories } = require('@mooncatrescue/contracts/moonCatUtils');
const { initializeFile, writeFileAsync } = require('../lib/fileUtils');

async function main() {
  const ACCESSORIES = await MoonCatAccessories;
  const OUTPUT_FILE = 'accessory_purchases.json';
  initializeFile(OUTPUT_FILE);
  let accessoryPurchases = require('../' + OUTPUT_FILE);
  if (typeof accessoryPurchases.purchases == 'undefined') {
    accessoryPurchases.purchases = {};
  }

  // Accessories contract deployed at https://etherscan.io/tx/0x21b831be284a48d6f2d72b84e94cfdd8123ec4b28779b098e8bc8caa7f02755a,
  // Block 12745799
  // Contract was unfrozen at https://etherscan.io/tx/0x0b8192a6dee25927cf7e5d2a3af072ed965c9bdd0cf668b56082ffc1df0128d6
  // Block 12782843
  let startBlock = (typeof accessoryPurchases.lastBlock == 'undefined') ? 12782843 : accessoryPurchases.lastBlock - 50;
  if (startBlock < 12782843) startBlock = 12782843;
  console.log(`Finding events starting at block ${startBlock}...`);

  // Fill purchase structure
  let totalAccessories = await ACCESSORIES.totalAccessories();
  for (let i = 0; i < totalAccessories; i++) {
    if (typeof accessoryPurchases.purchases[i] == 'undefined') {
      accessoryPurchases.purchases[i] = {};
    }
  }

  // Find events that were fired recently from the contracts
  const eventFilter = {
    address: ACCESSORIES.address,
    topics: [[
      ethers.utils.id('AccessoryApplied(uint256,uint256,uint8,uint16)'),
      ethers.utils.id('AccessoryPurchased(uint256,uint256,uint256)'),
    ]]
  };
  //console.log(ACCESSORIES.interface.events);

  let logs = await ACCESSORIES.queryFilter(eventFilter, startBlock, startBlock + 150000);
  if (logs.length == 0) {
    console.log('No events found after that block');
    process.exit(0);
  }

  let curBlock = startBlock;
  console.log(`${logs.length} events needing to be parsed:`)
  for(let i = 0; i < logs.length; i++) {
    const curLog = logs[i];
    let event = ACCESSORIES.interface.parseLog(curLog);
    let rescueOrder = event.args.rescueOrder.toNumber();
    let accessoryId = event.args.accessoryId.toNumber();
    if (typeof accessoryPurchases.purchases[accessoryId][rescueOrder] == 'undefined') {
      let block = await curLog.getBlock();
      let blockTime = new Date(block.timestamp * 1000);
      let tx = await curLog.getTransactionReceipt();
      accessoryPurchases.purchases[accessoryId][rescueOrder] = {
        block: {
          number: curLog.blockNumber,
          hash: curLog.blockHash
        },
        timestamp: block.timestamp,
        tx: tx.transactionHash,
        sender: tx.from,
      };
      console.log(`${logs.length - i} - ${event.name}: MoonCat #${rescueOrder} bought Accessory #${accessoryId}, in block ${curLog.blockNumber} (${blockTime.toLocaleString()})`);
    }
    if (curLog.blockNumber != curBlock) {
      accessoryPurchases.lastBlock = curBlock;
      curBlock = curLog.blockNumber;
      await writeFileAsync(OUTPUT_FILE, JSON.stringify(accessoryPurchases, null, 2));
    }
  }

  accessoryPurchases.lastBlock = curBlock;
  await writeFileAsync(OUTPUT_FILE, JSON.stringify(accessoryPurchases, null, 2));
  console.log('Done!');
}

main()
.then(() => process.exit(0))
.catch(error => {
  console.error(error);
  process.exit(1);
});
