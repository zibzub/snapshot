require('dotenv').config();
const { ethers } = require('hardhat');
const { writeFileAsync } = require('../lib/fileUtils');
const { MoonCatRescue } = require('@mooncatrescue/contracts/moonCatUtils');

// https://medium.com/better-programming/solidity-storage-variables-with-ethers-js-ca3c7e2c2a64

// MoonCatRescue memory slot numbering:
// ===================================
//        0: Contract Mode
//        0: Owner Address
//        1: Parser MD5
//        2: Token Name ("MoonCats")
//        3: Token Symbol ("🐱")
//        4: Decimals (0)
//        5: Total Supply
//        6: Remaining Cats
//        6: Remaining Genesis
//        6: Rescue Index
//   7-4273: MoonCat IDs (uint16 x 25600; 6/slot)
//     4274: Search seed
//     4275: Adpotion Offers
//     4276: Adoption Requests
//     4277: MoonCat Names
//     4278: MoonCat Owners
//     4279: Owner Balances
//     4280: Pending Withdrawals

const abi = ethers.utils.defaultAbiCoder;

/**
 * Generate a `${MCR.address}_memory.json` cache file for faster ownership lookups.
 * Rather than looping through calling "catOwners()" for each MoonCat (having the EVM look up
 * the data in memory, parse, and format it), record where in smart contract memory that data is
 * stored (which doesn't change). With that information generated, ownership can be determined by
 * looking up the raw memory value (a faster RPC call).
 */
async function main() {
  // For each MoonCat, note their ID, and then calculate the memory storage slot of their metadata
  const MCR = await MoonCatRescue;

  const MOONCATRESCUE_MEMORY_SLOTS_FILE = `${MCR.address}_memory.json`;
  let moonCatRescue_memory = [];

  for (let rescueOrder = 0; rescueOrder < 25440; rescueOrder++) {
    let data = {
      rescueOrder: rescueOrder,
      catId: await MCR.rescueOrder(rescueOrder)
    };
    data.adoptionOfferSlot = ethers.utils.keccak256(abi.encode(['bytes5', 'uint256'], [data.catId, 4275]));
    data.adoptionRequestSlot = ethers.utils.keccak256(abi.encode(['bytes5', 'uint256'], [data.catId, 4276]));
    data.nameSlot = ethers.utils.keccak256(abi.encode(['bytes5', 'uint256'], [data.catId, 4277]));
    data.ownerSlot = ethers.utils.keccak256(abi.encode(['bytes5', 'uint256'], [data.catId, 4278]));
    if (rescueOrder % 500 == 0) {
      let storageData = await ethers.provider.getStorageAt(MCR.address, data.ownerSlot);
      console.log (rescueOrder, data.catId, storageData);
    }

    moonCatRescue_memory[data.rescueOrder] = data;
  }
  await writeFileAsync(MOONCATRESCUE_MEMORY_SLOTS_FILE, JSON.stringify(moonCatRescue_memory, null, 2));
  console.log('Done!');

}

main()
.then(() => process.exit(0))
.catch(error => {
  console.error(error);
  process.exit(1);
});
