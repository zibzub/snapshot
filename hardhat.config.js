require('dotenv').config();
require("@nomiclabs/hardhat-ethers");

const { API_URL, ETHERSCAN_API_KEY, FORK_BLOCK } = process.env
if (typeof API_URL == 'undefined') {
  // Make this error a bit more evident. If we don't manually check,
  // the message Hardhat gives isn't that helpful to determine the root cause
  console.error('No API_URL defined!');
  process.exit(1);
}

if (typeof FORK_BLOCK == 'undefined') {
  console.error('No Fork block defined');
  process.exit(1);
}

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  defaultNetwork: "hardhat",
  etherscan: {
    apiKey: ETHERSCAN_API_KEY
  },
  networks: {
    hardhat: {
      chainId: 1337,
      allowUnlimitedContractSize: true,
      forking: {
        url: API_URL,
        blockNumber: parseInt(FORK_BLOCK)
      }
    }
  },
  solidity: {
    compilers: [
      {
        version: "0.8.12",
        settings: {
          optimizer: {
            enabled: true,
            runs: 200
          }
        }
      }
    ]
  },
  mocha: {
    timeout: 60000,
    slow: 50000
  }
};
