require('dotenv').config();
const fs = require('fs');
const { MoonCatRescue } = require('@mooncatrescue/contracts/moonCatUtils');
const { ethers } = require('ethers');
const traits = require('./mooncat_traits.json');
const { initializeFile, writeFileAsync } = require('./lib/fileUtils');

const { API_URL } = process.env;
const START_BLOCK = 4140409; // https://etherscan.io/tx/0xd31d05adb302131f0c31f1a001685f29eb3b2b66d2af3b90d1e2c7f22661db61
const STOP_BLOCK = 12025380
const STEP_SIZE = 2_000;
const OUTPUT_FILE = 'mooncat_rescues.json';

function getRescueOrder(catId) {
  for(let i = 0; i < traits.length; i++) {
    if (traits[i].catId == catId) return traits[i].rescueOrder;
  }
  return false;
}

(async () => {
  const provider = new ethers.providers.JsonRpcProvider(API_URL);
  const RESCUE = (await MoonCatRescue).connect(provider);

  let curBlock = START_BLOCK;

  initializeFile(OUTPUT_FILE);
  let rescues = require('./' + OUTPUT_FILE);
  let blockTimes = {};

  let doneIds = Object.keys(rescues);
  if (doneIds.length > 0) {
    curBlock = rescues[doneIds[doneIds.length - 1]].blockHeight
  }

  while (curBlock < STOP_BLOCK) {
    console.log('at', curBlock);
    let logs = await RESCUE.queryFilter('CatRescued', curBlock, curBlock + STEP_SIZE - 1);
    console.log(`Found ${logs.length} logs to parse...`)
    for (let i = 0; i < logs.length; i++) {
      let log = logs[i];
      if (typeof blockTimes[log.blockNumber] == 'undefined') {
        let block = await log.getBlock();
        blockTimes[log.blockNumber] = block.timestamp;
      }
      let tx = await log.getTransaction();
      let seed = '0x' + tx.data.slice(10);
      rescues[log.args.catId] = {
        txHash: log.transactionHash,
        blockHeight: log.blockNumber,
        timestamp: blockTimes[log.blockNumber],
        rescueOrder: getRescueOrder(log.args.catId),
        seed: seed,
        catId: log.args.catId,
        rescuer: log.args.to
      };
      if (i > 0 && i % 10 == 0) process.stdout.write('.');
      if (i > 0 && i % 100 == 0) {
        console.log('');
        await writeFileAsync(OUTPUT_FILE, JSON.stringify(rescues, null, 2));
      }
      if (getRescueOrder(log.args.catId) == 25439) {
        await writeFileAsync(OUTPUT_FILE, JSON.stringify(rescues, null, 2));
        console.log('Done!');
        process.exit(0);
      }
    }

    curBlock += STEP_SIZE;
    console.log('');
    await writeFileAsync(OUTPUT_FILE, JSON.stringify(rescues, null, 2));
  }
})();
